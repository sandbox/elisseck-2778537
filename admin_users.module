<?php

/**
 * @file
 * A block module that displays admin users.
 */

/**
 * Implements hook_help().
 *
 * Displays help and module information.
 *
 * @param path
 *   Which path of the site we're using to display help
 * @param arg
 *   Array that holds the current path as returned from arg() function
 */
function admin_users_help($path, $arg) {
  switch ($path) {
    case "admin/help#admin_users":
      return '' . t("Displays links to admin users") . '';
      break;
  }
}

/**
 * Implements hook_block_info().
 */
function admin_users_block_info() {
  $blocks['admin_users'] = array(
    // The name that will appear in the block list.
    'info' => t('Admin Users'),
    // Default setting.
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  return $blocks;
}

/**
 * Custom users function.
 * @return
 *   A result set of admin users.
 */
function admin_users_contents($display){
    $user_select = variable_get('admin_users_role', 3);
    $query = db_select('users', 'u');
    $query->fields('u', array('uid', 'name'));
    $query->innerJoin('users_roles', 'r', 'r.uid = u.uid');
    //$query->condition('r.rid', $rid);
    $query->condition('r.rid', $user_select);
    $query->orderBy('u.name');
    if ($display == 'block'){
      // Restrict the range if called with 'block' argument.
      $query->range(0, 10);
      } //Now proceeds to execute().
    //If called by page, query proceeds directly to execute().
    
      $result = $query->execute();
  return $result;
}

function admin_users_filter(){
  $result = drupal_get_form('admin_users_form');
  return $result;
}

/**
 * Implements hook_block_view().
 *
 * Prepares the contents of the block.
 */
function admin_users_block_view($delta = '') {
  switch ($delta) {
    case 'admin_users':
        $block['subject'] = t('Select User Role to List');
        if (user_access('access content')) {
          // Use our custom function to retrieve data.
        $block['content'] = admin_users_filter('block');
        //$block['content'] = t('No such users available.');
        }
    return $block;
      break;
  }
}
/**
 * Implements hook_menu().
 */
function admin_users_menu() {
  $items = array();
  $items['admin_users'] = array(
        'title' => 'Admin Users',
        'page callback' => '_admin_users_page',
        'access arguments' => array('access admin_users content'),
        'type' => MENU_NORMAL_ITEM, //Will appear in Navigation menu.
      );
    
  $items['admin/config/people/admin_users'] = array(
    'title' => 'Admin Users',
    'description' => 'Configuration for Admin Users module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('admin_users_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );
 
  return $items;
}

/**
 * Custom function
 *
 * Query db for possible roles for configuration options and format for select list options
 *
 * @return
 * A result set of non-reserved roles that exist formatted as object
 */
 
function roles_query() {
    $query = db_select('role', 'r');
    $query->fields('r', array('rid','name'));
    $query->condition('r.rid', 2, '>');
    $result = $query->execute()->fetchAllKeyed();
    return $result;
}
/**
 * Page callback: Admin list settings
 *
 * @see current_posts_menu()
 */
function admin_users_form($form, &$form_state) {
  $role_values = roles_query();   //MODIFIED LINE 1/2
  $form['role_options'] = array(
    '#type' => 'value',
    '#value' => $role_values //MODIFIED LINE 2/2
  );
  $form['admin_users_role'] = array(
    '#type' => 'select',
    '#title' => t('User role to list'),
    '#default_value' => variable_get('admin_users_role', 3),
    '#options' => $form['role_options']['#value'],
    '#description' => t('The user role to display in the block.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

function admin_users_filter_form($form, &$form_state) {
 
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Select Role'),
  );
 
  return $form;
}

/**
 * Implements validation from the Form API.
 *
 * @param $form
 *   A structured array containing the elements and properties of the form.
 * @param $form_state
 *   An array that stores information about the form's current state
 *   during processing.
 */
function admin_users_form_validate($form, &$form_state){
  $users_role = $form_state['values']['admin_users_role'];
  if (!is_numeric($users_role)){
    form_set_error('admin_users_role', t('You must enter a number for the users role to display.'));
  }
  elseif ($users_role <= 0){
    form_set_error('admin_users_role', t('Users role must be positive.'));
  }
}

/**
 * Implements hook_permission().
 */
function admin_users_permission() {
  return array(
    'access admin_users content' => array(
      'title' => t('Access content for the Admin Users module'),
    )
  );
}

/**
 * Custom page callback function, declared in current_posts_menu().
 */
function _admin_users_page() {
  drupal_set_title('Selected Users');
  $result = admin_users_contents('page');
  //Array to contain items for the page to render.
  $items = array();
  //Iterate over the resultset and format as links.
  foreach ($result as $user_role) {
    $items[] = array(
    'data' => l($user_role->name, 'user/' . $user_role->uid),
       );
    }
  if (empty($items)) { //No users in this category
    $form = drupal_get_form('admin_users_form');
    print drupal_render($form);
    $page_array['admin_users_arguments'] = array(
      //Title serves as page subtitle
      '#title' => t('All users in this role'),
      '#markup' => t('No users available.'),
    );
    return $page_array;  
  }
    else {
      $page_array['admin_users_arguments'] = array(
      '#title' => t('All users in this role'),
      '#items' => $items,
      //Theme hook with suggestion.  
      '#theme' => 'item_list__admin_users',
    );
  }
return $page_array;
}

function admin_users_page_alter(&$page) {
  if (isset($page['sidebar_first'])) {
    $page['highlighted']['admin_users_admin_users'] = $page['sidebar_first']['admin_users_admin_users'];
    unset($page['sidebar_first']['admin_users_admin_users']);
  }
}
